
function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card shadow mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer"><time>${starts} - ${ends}</time></div>
      </div>
    `;
  }

  function alert() {
    return `
    <div class = "container">
      <div class="alert alert-danger" role="alert">
        <h5>This is an error!<h5>
        <p>
            If you are reading this error message, it is likely because the person who
            who wrote it is new ad coding. Please contact him!
        </p>
      </div>
    </div>
    `;
  }



  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');
    let colIndx = 0

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Response not ok');
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const starts = new Date(details.conference.starts).toLocaleDateString();
            const ends = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            const column = columns[colIndx % 3] //document.querySelector('.col');
            column.innerHTML += html;
            colIndx = (colIndx + 1) % 3
            }
        }

      }
    } catch (e) {
        console.error('error', e)  // Figure out what to do if an error is raised
        const errorAlert = alert();
        const container = document.querySelector('.container');
        container.insertAdjacentHTML('afterbegin', errorAlert);
    }

  });

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;


//         for (let conference of data.conferences) {
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           const descriptionTag = document.querySelector('.card-text');
//           descriptionTag.innerHTML = details.conference.description;
//           console.log(details);

//           const imageTag = document.querySelector('.card-img-top');
//           imageTag.src = details.conference.location.picture_url;


//           console.log(details);
//         }

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });


// const descriptionTag = document.querySelector('.card-title');
// descriptionTag.innerHTML = conference.description;

// const imageTag = document.querySelector('.card-img-top');
// imageTag.src = details.conference.location.picture_url;


//throw new Error('Response not ok');
//console.error('error', error);
