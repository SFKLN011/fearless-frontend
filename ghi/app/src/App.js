import { BrowserRouter, Routes, Route } from "react-router-dom";

import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div>
        <Routes>
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/conferences/new" element={<ConferenceForm />} />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="/presentation/new" element={<PresentationForm />}/>
          <Route index element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
